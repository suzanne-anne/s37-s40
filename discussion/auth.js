const jwt = require("jsonwebtoken")
const secret = "B271CourseBookingAPI" //Secret key to be used for validating the token

// Method for generating a token with JWT
module.exports.createAccessToken = (user) => {
	const user_data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(user_data, secret, {})
}

module.exports.verify = (request, response, next) => {
	let token = request.headers.authorization

	if(typeof token !== "undefined"){
		// If the token exists, then slice its first 7 characters to remove the default 'Bearer ' text when using the request.headers.authorization property. What will be left if only the token itself.
		token = token.slice(7, token.length)


		// The verify function will check the token and the secret key used in this application to verify where it came from. 
		return jwt.verify(token, secret, (error, data) => {
			// After verification if there is an error, it will return an object saying that the authentication has failed
			if(error){
				return response.send({auth: "Verification failed."})
			}

			// But if there is no error, then it will exit this function and move on to the next function
			next()
		})
	} else {
		return response.send({auth: "Token is not defined."})
	}
} 

module.exports.decode = (token) => {
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length)

		// Before decoding and getting the user data from the token, it first will verify it using the secret key
		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return null
			}

			// After token has been verified, it runs the decode function from JWT which will then be able to access its payload (user data)
			return jwt.decode(token, {complete: true}).payload
		})
	} else {
		return null
	}
}