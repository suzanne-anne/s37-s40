// Setup dependencies
const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
require('dotenv').config() // Initialize ENV
const user_routes = require("./routes/userRoutes")

// MongoDB Connection
mongoose.connect(`mongodb+srv://earlrafael_:${process.env.MONGODB_PASSWORD}@cluster0.0dhrt.mongodb.net/course-booking-db?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once('open', () => console.log("Connected to MongoDB!"))

// Server Setup
const app = express()

// Middleware
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// Routes
app.use("/users", user_routes)

// Server Listening
// To dynamically switch from the port of the localhost to the port of the cloud server, we have to use an OR operator to check which one express.js will use
app.listen(process.env.PORT || 3000, () => {
	console.log(`API is now running on port ${process.env.PORT || 3000}`)
})